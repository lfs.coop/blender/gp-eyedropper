import bpy
from bpy.types import PropertyGroup
from bpy.props import *
from . eyedropper_maths import OnClickDataPicker, OnFlyDataPicker
from . eyedropper_draw import draw_callback_px
from . eyedropper_props import GPEYEDROPPER_PickedItem

class GPEYEDROPPER_UL_PickedList(bpy.types.UIList):
    bl_idname = "GPEYEDROPPER_UL_PickedList"

    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        ob, lay, mat = item.object, item.layer, item.material
        mat_icon = bpy.data.materials[mat].id_data.preview.icon_id
        self.use_filter_show = False

        act_ind = getattr(active_data, active_propname)
        is_selected = (act_ind == index)
        if (item.selected != is_selected):
            item.selected = is_selected

        emboss_ops = True
        layout.emboss='PULLDOWN_MENU'
        layout.ui_units_y = 1.1

        col = layout.column()   
        col.enabled = True
        col.label(text=f'{ob}')

        col = layout.column()   
        col.enabled = True
        col.label(text=f'{lay}')

        col = layout.column()   
        col.alignment = 'RIGHT'
        row = col.row()
        row.alignment = 'RIGHT'

        layer = item.get_layer()
        row.prop(layer, "hide", text='', emboss=False)
        row.prop(item, "selected_layonly", text=f'', emboss=emboss_ops, icon="GREASEPENCIL")
        row.prop(item, "selected_matonly", text='', icon_value=mat_icon)

class GPEYEDROPPER_OT_eyeDropperList(bpy.types.Operator):
    bl_idname = "scene.gp_eyedropper_list"
    bl_label = "GP Eyedropper List"  
    bl_options = {'REGISTER', 'UNDO'}

    def update_selection(self, context):
        pass

    dev_mode: BoolProperty(default=False)
    active_index: IntProperty(default=-1, update=update_selection)
    picked_list: CollectionProperty(type=GPEYEDROPPER_PickedItem)

    def pick_layer(self, context):
        odp = self.object_data_picker
        picked = []

        def merge_list(all_picked, picked_in_ob):
            merged = all_picked
            def insert_picked(p, i0, i1):
                nonlocal merged
                if(i0 == i1):
                    merged.append(p)
                    return len(merged)-1
                for i in range(i0, i1):
                    if merged[i][0] > p[0]:
                        merged.insert(i,p)
                        return i
                merged.append(p)
                return len(merged)-1
            i0, i1 = 0, len(merged)
            for p in picked_in_ob:
                i0 = insert_picked(p,i0,i1)+1
                i1 += 1
            return merged

        for ob in context.visible_objects:
            if (ob.type != 'GPENCIL') \
                or (not odp.pick(ob, self.cursor, context)):
                continue

            picked_in_object = []
            for lay_id, lay_dat in odp.picked.items():
                lay = ob.data.layers[lay_id]

                for stk_id, stk_dat in lay_dat.items():
                    z_depth = odp.get_depth_result(context, lay_id, stk_id)

                    stk = lay.active_frame.strokes[stk_id]
                    mat_name = ob.material_slots[stk.material_index].material.name
                    picked_in_object += [(z_depth, ob.name, lay.info, mat_name) ]
            
            picked = merge_list(picked, picked_in_object)     

        def already_picked(ob, lay, mat):
            for picked in self.picked_list:
                if (picked.layer == lay) \
                    and (picked.material == mat) \
                    and (picked.object == ob):
                    return True
            return False
        
        self.picked_list.clear()
        for (z,ob,lay,mat) in picked:
            if not already_picked(ob, lay, mat):
                picked_item = self.picked_list.add()
                picked_item.object = ob
                picked_item.layer = lay
                picked_item.material = mat

    def execute(self, context):
        return {'FINISHED'}

    def draw(self, context):
        row = self.layout.row()
        row.template_list("GPEYEDROPPER_UL_PickedList",'Picked_Items', \
                        dataptr=self, propname="picked_list", \
                        active_dataptr=self, active_propname="active_index", \
                        rows=len(self.picked_list), maxrows=len(self.picked_list), \
                        type='DEFAULT'
                        )

    def invoke(self, context, event):
        self.cursor = event.mouse_region_x, event.mouse_region_y
        self.object_data_picker = OnClickDataPicker(context,list_data=True,record_time=self.dev_mode)

        self.pick_layer(context)

        if self.dev_mode and self.object_data_picker:
            print(f'Time stats : {self.object_data_picker.get_record_stats()}')
        
        self.hard_max=len(self.picked_list)
        self.object = context.active_object
        wm = context.window_manager
        wm.invoke_popup(self)

        return {'RUNNING_MODAL'}


class GPEYEDROPPER_OT_eyeDropper(bpy.types.Operator):
    bl_idname = "scene.gp_eyedropper"
    bl_label = "GP Eyedropper"  

    picked_list: CollectionProperty(type=GPEYEDROPPER_PickedItem)

    def pick_layer(self, context):
        odp = self.object_data_picker   
        z_min = None
        self.picked_list.clear()
        
        for ob in context.visible_objects:
            if ob.type != 'GPENCIL':
                continue
            
            if not odp.pick(ob, self.cursor, context):
                continue

            for lay_id, lay_dat in odp.picked.items():
                lay = ob.data.layers[lay_id]
                
                for stk_id, stk_dat in lay_dat.items():
                    z_depth = odp.get_depth_result(context, lay_id, stk_id)

                    if (not z_min is None) \
                        and (z_depth > z_min):
                        continue

                    z_min = z_depth
            
                    is_line = stk_dat["is_line"]
                    seg_id = stk_dat["seg_id"]

                    stk = lay.active_frame.strokes[stk_id]
                    mat_name = ob.material_slots[stk.material_index].material.name

                    if (not self.picked_list):
                        picked = self.picked_list.add()
                    else:
                        picked = self.picked_list[0]

                    if picked:
                        picked.update(ob.name,lay.info, mat_name, stk_id, seg_id, is_line)

    def clear_handler(self):
        bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
        if self.dev_mode and (not self.onclick_mode):
            print(f'Time stats : {self.object_data_picker.get_record_stats()}')

    def modal(self, context, event):  
        ivk_rls =(event.type == self.invoke_key) and (event.value == 'RELEASE')
        self.cursor = event.mouse_region_x, event.mouse_region_y

        if ((event.type == self.invoke_key) and (event.value == 'PRESS')) \
            or (event.type == 'MOUSEMOVE'):
            if self.onclick_mode:
                self.onclick_mode = False
                self.object_data_picker = OnFlyDataPicker(context,list_data=False,record_time=self.dev_mode)
            self.pick_layer(context)   
            context.area.tag_redraw() 
            return {'RUNNING_MODAL'}

        if (self.onclick_mode and ivk_rls):
            self.clear_handler()
            bpy.ops.scene.gp_eyedropper_list('INVOKE_DEFAULT', dev_mode=self.dev_mode)
            return {'FINISHED'}

        if (event.type == 'RIGHTMOUSE') \
            or ((not self.onclick_mode) and ivk_rls):
            self.clear_handler()
            context.area.tag_redraw() 
            return {'FINISHED'}        
            
        if (event.type == 'LEFTMOUSE' and event.value == 'PRESS'):  
            self.clear_handler() 
            if not self.picked_list:
                return {'FINISHED'}
            self.picked_list[0].selected = True
            return {'FINISHED'}
        
        return {'RUNNING_MODAL'}

    def invoke(self, context, event):  
        # Get addon preferences
        pname = (__package__).split('.')[0]
        addon_prefs = context.preferences.addons[pname].preferences
        if addon_prefs is None : 
            self.report({'WARNING'}, "Could not load user preferences, running with default values")
            self.dev_mode = False
        else:
            self.dev_mode = addon_prefs.developer_mode
        self.invoke_key = 'D'    
        self.onclick_mode = True
        self.cursor = event.mouse_region_x, event.mouse_region_y
        
        wm = context.window_manager
        sd = context.space_data

        wm.modal_handler_add(self)   
        self._handle = sd.draw_handler_add(draw_callback_px, (self,context), 'WINDOW', 'POST_PIXEL')
        return {'RUNNING_MODAL'}

classes = (GPEYEDROPPER_UL_PickedList, GPEYEDROPPER_OT_eyeDropperList, GPEYEDROPPER_OT_eyeDropper)
def register(addon_keymaps):
    for cls in classes:
        bpy.utils.register_class(cls) 

    kmi_type = 'D'
    kmi_ctrl, kmi_alt, kmi_shift = False, False, False

    wm = bpy.context.window_manager
    kc = wm.keyconfigs.addon
    if kc:
        # Add keymap items
        km = wm.keyconfigs.addon.keymaps.new(name='3D View', space_type='VIEW_3D')
        kmi = km.keymap_items.new(GPEYEDROPPER_OT_eyeDropper.bl_idname, value='PRESS', \
                                type=kmi_type, ctrl=kmi_ctrl, alt=kmi_alt, shift = kmi_shift)
        addon_keymaps.append((km, kmi, "Invoke Picker"))

def unregister():        
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
import bpy
from mathutils import Vector
from mathutils.geometry import intersect_point_tri_2d, intersect_point_line
from bpy_extras.view3d_utils import location_3d_to_region_2d, region_2d_to_origin_3d
import time

def gp_material(ob, stk):
    return ob.material_slots[stk.material_index].material.grease_pencil

def get_local_co(ob, lay, stk, context):
    mat = ob.matrix_world
    show_stroke = gp_material(ob,stk).show_stroke

    def vert_local_co(co):
        return location_3d_to_region_2d(context.region, \
            context.space_data.region_3d, mat @ co)

    def get_vert_radius(i,v):
        # based on https://developer.blender.org/T85204

        line_change = lay.line_change
        stk_thickness = stk.line_width
        pressure = v.pressure
        pixel_factor = ob.data.pixel_factor

        wdl_rds = ((line_change + stk_thickness)*pressure*pixel_factor)/1000.0

        tangent_vec = stk.points[i].co - stk.points[i-1].co

        s0 = vert_local_co(v.co)
        s1 = vert_local_co(v.co + wdl_rds*tangent_vec.normalized())

        if (s0 is None) or (s1 is None):
            return None
        
        loc_rds = (s0-s1).length/2.

        return loc_rds

    return [ (vert_local_co(v.co),get_vert_radius(i,v)) \
        if show_stroke else (vert_local_co(v.co),0) \
        for i,v in enumerate(stk.points)]

def get_point_bbox(pts):
    return [min( [ co[0]-rds for co,rds in pts ] ), \
            min( [ co[1]-rds for co,rds in pts ] ), \
            max( [ co[0]+rds for co,rds in pts ] ), \
            max( [ co[1]+rds for co,rds in pts ] )
            ]

def merge_bboxes(bbox1, bbox2):
    def _min(x,y):
        if x is None:
            return y
        return min(x,y)

    def _max(x,y):
        if x is None:
            return y
        return max(x,y)

    for xy in range(2):
        bbox1[xy] = _min(bbox1[xy], bbox2[xy])
        bbox1[2+xy] = _max(bbox1[2+xy], bbox2[2+xy])

class ObjectDataPicker():
    ob=None
    cursor=Vector((0,0))
    layers={}
    picked={}

    def __init__(self, context, list_data=False):
        self.list_data = list_data
        self.layers = { ob.name:{id:lay for id,lay in enumerate(ob.data.layers) if (not lay.hide) and (lay.active_frame)}\
                        for ob in bpy.data.objects \
                        if (not ob is None) and (not ob.hide_get()) and (ob.type == 'GPENCIL')  }

        self.clear_results()

    def clear_results(self):
        self.found_res = False
        self.picked = {}

    def set_result(self, lid, sid, tid, coef, is_line):
        self.found_res = True
        
        picked = self.picked
        if not lid in picked:
            picked[lid] = {}

        if sid in picked[lid]:
            # the stroke was already identified
            return False
        
        picked[lid][sid] = {"seg_id":tid,"coef":coef,"is_line":is_line}

    def pick(self, ob, cursor, context):
        self.ob = ob
        self.cursor = cursor
        return None

    def get_3D_point(self, layer_id, stroke_id):
        if (not self.found_res) \
            or (not layer_id in self.picked) \
            or (not stroke_id in self.picked[layer_id]):
            return None

        picked = self.picked[layer_id][stroke_id]
        is_line = picked["is_line"]
        coef = picked["coef"]
        seg_id = picked["seg_id"]

        lay = self.layers[self.ob.name][layer_id]
        stk = lay.active_frame.strokes[stroke_id]

        if is_line:
            s0, s1 = stk.points[seg_id], stk.points[seg_id+1]
            return (1.-coef)*s0.co + coef*s1.co

        (a,b,c) = coef
        tri = stk.triangles[seg_id]
        tri_co = [ stk.points[v].co for v in [tri.v1, tri.v2, tri.v3] ]
        return a*tri_co[0] + b*tri_co[1] + c*tri_co[2]

    def get_depth_result(self, context, layer_id, stroke_id):
        if not self.found_res:
            return None

        mat = self.ob.matrix_world
        pt = mat @ self.get_3D_point(layer_id, stroke_id)

        reg, r3d = context.region, context.space_data.region_3d
        org = 0.5*Vector((reg.width, reg.height))

        ''' @amelie: not sure this function actually computes the depth in 3D
            because of this call to region_2d_to_origin_3d, idk what to put in org
            BUT putting the same org for all object 
            seems to be good enough for depth comparison
        '''
        org3d = region_2d_to_origin_3d(reg, r3d, org)

        return (pt-org3d).length

    def pick_under(self, stk_pts, stk_tri):
        def is_inside_tri(tri):
            return intersect_point_tri_2d(self.cursor, tri[0], tri[1], tri[2])

        def barycentric_coords(tri):
            from mathutils.geometry import area_tri
            a,b,c = tri[0], tri[1], tri[2]
            area = area_tri(a,b,c)
            beta = area_tri(a,self.cursor,c)/area
            gamma = area_tri(a,b,self.cursor)/area
            return (1.-beta-gamma), beta, gamma

        def is_inside_seg(s0,s1,r0,r1):
            cpt, dst = intersect_point_line(self.cursor, s0, s1)
            if (dst < 0) or (dst > 1):
                return False, -1
            rds = (1. - dst)*r0 + dst*r1
            D = (cpt-Vector(self.cursor)).length
            return (D < rds), dst

        # Strokes
        for i in range(len(stk_pts)-1):
            s0,r0 = stk_pts[i]
            s1,r1 = stk_pts[i+1]
            inside,d = is_inside_seg(s0,s1,r0,r1)
            if inside:
                return i,d,True

        # Fills
        for tid,co in stk_tri:
            if is_inside_tri(co):
                bc = barycentric_coords(co)
                return tid, bc, False
        
        return None

    
    def is_inside_bbx(self, bbx):
        return  (self.cursor[0] >= bbx[0]) \
            and (self.cursor[1] >= bbx[1]) \
            and (self.cursor[0] <= bbx[2]) \
            and (self.cursor[1] <= bbx[3])

class TimedDataPicker(ObjectDataPicker):
    def init(self, context):    
        self.nlays, self.nstks, self.ntris = 0, 0, 0

        for lays in self.layers.values():
            self.nlays += len(lays)
            stks = [lay.active_frame.strokes for lay in lays.values() \
                if (not lay.hide) and (lay.active_frame)]
            self.nstks += sum([len(stk) for stk in stks])
            tris = [ s.triangles for stk in stks for s in stk]
            self.ntris += sum([len(tri) for tri in tris])

    def __init__(self, context, list_data=False, record_time=False):
        super().__init__(context, list_data)

        self.is_time_recorded = record_time
        if self.is_time_recorded:
            self.time_record = []
        
        tstart = time.time()
        self.init(context)
        self.preproc_time = time.time() - tstart

    def _pick(self, context):
        return None

    def pick(self, ob, cursor, context):
        super().pick(ob, cursor, context)

        if not self.is_time_recorded:
            return self._pick(context)

        tsart = time.time()
        result = self._pick(context)
        self.time_record.append(time.time() - tsart)  
        
        return result

    def get_custom_stats(self):
        return {}

    def get_record_stats(self):
        if not self.is_time_recorded:
            return None    
        
        stats = {}

        stats["preproc_time"] = self.preproc_time
        
        if self.time_record:
            stats["mean_time"] = sum(self.time_record)/len(self.time_record)
          
        stats["nlayers"] = self.nlays
        stats["nstrokes"] = self.nstks
        stats["ntris"] = self.ntris

        stats.update(self.get_custom_stats())
        
        return stats

class OnClickDataPicker(TimedDataPicker):

    def _pick(self, context):
        ob = self.ob
        if (ob is None) or (not ob.name in self.layers):
            return False

        self.clear_results()
        lays = self.layers[ob.name]        
        for lid in sorted(lays.keys(), reverse=True):
            lay = lays[lid]
            
            sid = len(lay.active_frame.strokes)
            for stk in reversed(lay.active_frame.strokes):
                sid -= 1

                show_stroke = gp_material(ob,stk).show_stroke
                show_fill = gp_material(ob,stk).show_fill

                if (not show_fill) and (not show_stroke):
                    continue

                local_pts = get_local_co(ob,lay,stk,context)
                
                if any([ (p is None) for p,x in local_pts]):
                    continue

                bbx = get_point_bbox(local_pts)
                
                if not self.is_inside_bbx(bbx):
                    continue

                tri_co = []
                if show_fill:
                    tri_co = [ (i,[local_pts[v][0] for v in [tri.v1,tri.v2,tri.v3]]) \
                                for i,tri in enumerate(stk.triangles) ]

                if not show_stroke:
                    local_pts = []

                pick_res = self.pick_under(local_pts, tri_co)
                if pick_res is None:
                    continue
                
                self.set_result(lid, sid, *pick_res)
                if not self.list_data:
                    return True

        return self.found_res


class OnFlyDataPicker(TimedDataPicker):
    def init(self, context):
        self.mem_data, self.nstks, self.ntris = self.preproc_local_coords(context)            
        self.nlays = len(self.mem_data)

    def preproc_local_coords(self,context):
        mem_data = {}
        lays = self.layers
        lid = len(lays)
        nstk, ntri = 0, 0

        for ob_name,lays in self.layers.items():
            ob = bpy.data.objects[ob_name]

            mem_ob = {}
            for lid in sorted(lays.keys(), reverse=True):
                lay = lays[lid]                

                lay_bbx = 4*[None]
                sid = len(lay.active_frame.strokes)
                mem_lay = {}
                
                for stk in reversed(lay.active_frame.strokes):
                    sid -= 1

                    show_stroke = gp_material(ob,stk).show_stroke
                    show_fill = gp_material(ob,stk).show_fill

                    if (not show_fill) and (not show_stroke):
                        continue

                    local_pts = get_local_co(ob,lay,stk,context)
                    
                    if any([ (p is None) for p,x in local_pts]):
                        continue

                    bbx = get_point_bbox(local_pts)
                
                    if show_fill:             
                        all_co = [ [local_pts[v][0] for v in [tri.v1,tri.v2,tri.v3]] for tri in stk.triangles ]
                        tri_co = [ (tid,co) for tid,co in enumerate(all_co) ]
                    else:
                        tri_co = []

                    if show_stroke:
                        locpts = local_pts
                    else:
                        locpts = []
                    
                    merge_bboxes(lay_bbx, bbx)
                    
                    mem_lay[sid] = (bbx, locpts, tri_co)

                    ntri += len(local_pts)

                if not mem_lay:
                    continue
                
                mem_ob[lid] = (lay_bbx, mem_lay)
                nstk += len(mem_lay)   

            if not mem_ob:
                continue

            mem_data[ob_name] = mem_ob         
        
        return mem_data, nstk, ntri
    
    def _pick(self, context):        
        ob = self.ob
        if (ob is None) or (not ob.name in self.mem_data):
            return False

        self.clear_results()
        mem_ob = self.mem_data[ob.name]
        for lid in mem_ob.keys():
            lbbx, mem_lay = mem_ob[lid]

            if not self.is_inside_bbx(lbbx):
                continue
            
            for sid in mem_lay.keys():
                sbbx, mem_stk, mem_fill = mem_lay[sid]

                if not self.is_inside_bbx(sbbx):
                    continue
                
                pick_res = self.pick_under(mem_stk, mem_fill)
                if pick_res is None:
                    continue

                self.set_result(lid, sid, *pick_res)
                if not self.list_data:
                    return True
                    
        return self.found_res
    

import gpu
from gpu_extras.presets import draw_circle_2d
from gpu_extras.batch import batch_for_shader
from bpy_extras.view3d_utils import location_3d_to_region_2d as loc2reg

from .. ui.ui_grid import *

def draw_triangles(context, ob, verts, tri, color = 3*[0.5] + [1.]):
    shader = gpu.shader.from_builtin('UNIFORM_COLOR')
    
    mat = ob.matrix_world 
    region, reg3D = context.region, context.space_data.region_3d
    pos = [ loc2reg(region, reg3D, mat @ p.co) for p in verts ]

    indices = [[(t.v1, t.v2), (t.v2, t.v3), (t.v3, t.v1)] for t in tri]
    indices = list({ k for x in indices for k in x })

    batch = batch_for_shader(shader, 'LINES', {"pos": pos}, indices=indices)
    shader.bind()     
    shader.uniform_float("color", color)

    batch.draw(shader)

def draw_segment(context, ob, verts, ids, color = 3*[0.5] + [1.]):
    shader = gpu.shader.from_builtin('UNIFORM_COLOR')
    
    mat = ob.matrix_world 
    region, reg3D = context.region, context.space_data.region_3d
    pos = [ loc2reg(region, reg3D, mat @ verts[i].co) for i in ids]

    indices = [(i,i+1) for i in range(len(ids)-1)]

    batch = batch_for_shader(shader, 'LINES', {"pos": pos}, indices=indices)
    shader.bind()     
    shader.uniform_float("color", color)

    batch.draw(shader)

def draw_vert_radius(context, ob, stk, pid):
    mat = ob.matrix_world
    def get_loc_vert(v):
        worldcoords = mat @ v
        return loc2reg(context.region, context.space_data.region_3d, worldcoords)

    point = stk.points[pid]
    radius = stk.line_width*point.pressure

    r3d = context.space_data.region_3d
    from mathutils import Vector
    rad_vec = Vector((radius, 0, 0, 0))
    rad_vec_2d = rad_vec @ r3d.perspective_matrix
    rad2d = rad_vec_2d.length

    print(f'Rad {radius} -> {rad2d}')

    radius = rad2d

    position = get_loc_vert(point.co)
    color = [1,0,0,1]
    draw_circle_2d(position, color, radius, segments=32)

def draw_callback_px(op, context):
    ''' On developer mode, display triangles or edges of stroke under the cursor '''
    def draw_debug(ob, stk, seg_id, is_line):
        if is_line:
            all_id = [*range(len(stk.points))]
            draw_segment(context, ob, stk.points, all_id, color =  3*[0.5] + [0.5])
            draw_segment(context, ob, stk.points, [seg_id, seg_id+1], color= [1.,1.,0.,1])
        else:
            draw_triangles(context, ob, stk.points, stk.triangles, color= 3*[0.5] + [0.2])
            draw_triangles(context, ob, stk.points, [stk.triangles[seg_id]], color= [1.,1.,0.,0.2])

    picked = op.picked_list

    if (op.dev_mode) and (picked):
        for pit in picked:
            draw_debug(pit.get_object(), pit.get_stroke(), pit.segment_id, pit.is_line)

    if picked:
        ob = picked[0].object
        lay = picked[0].layer
        mat = picked[0].get_material()
        stki = "(" + str(picked[0].stroke_id) + ")"
    else:
        ob = 'None'
        lay = 'None'
        mat = None
        stki = ''
    grid_theme = grid_theme_from_preferences(context)

    ncol = 4 if op.dev_mode else 3
    grid = UIGrid(ncol=ncol, nrow=1, theme=grid_theme)
    grid.set_text(0,0,ob)
    grid.set_text(0,1,lay)
    grid.set_material(0,2,mat)
    if op.dev_mode:
        grid.set_text(0, 3, stki)
    grid.draw(context, op.cursor)
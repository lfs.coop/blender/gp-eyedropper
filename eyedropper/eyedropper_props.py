import bpy
from bpy.types import PropertyGroup
from bpy.props import *

class GPEYEDROPPER_PickedItem(PropertyGroup):
    object: StringProperty(default="")
    layer: StringProperty(default="")
    material: StringProperty(default="")

    def select_item(self, context):
        if not self.selected:
            return
        self.select_matonly(context)        
        self.select_layonly(context)        

    def select_matonly(self, context):
        if not (self.selected or self.selected_matonly):
            return

        ob = self.get_object()
        mat = self.get_material()

        ob_mat = ob.data.materials
        if not self.material in ob_mat:
            ob_mat.append(mat)    
        
        mat_index = ob_mat.find(mat.name)
        ob.active_material_index = mat_index

    def select_layonly(self, context):
        if not (self.selected or self.selected_layonly):
            return
        
        ob = self.get_object()
        lay = self.get_layer()

        # Object selection
        mode = context.mode
        for _ob in context.selected_objects:
            _ob.select_set(False)
        ob.select_set(True)
        context.view_layer.objects.active = ob       

        try: 
            bpy.ops.object.mode_set(mode=mode)
        except RuntimeError:
            print(f"WARNING :Could not switch to {mode} mode")
        
        # Layer selection
        autolock = ob.data.use_autolock_layers
        if autolock:
            ob.data.layers.active.lock = True
            lay.lock = False
        ob.data.layers.active = lay

    selected: BoolProperty(default=False, update=select_item)
    selected_matonly: BoolProperty(default=False, update=select_matonly)
    selected_layonly: BoolProperty(default=False, update=select_layonly)

    stroke_id: IntProperty(default=-1)
    segment_id: IntProperty(default=-1)
    is_line: BoolProperty(default=False)

    def update(self, ob, lay, mat, stk_id, seg_id, is_line):
        self.object = ob
        self.layer = lay
        self.material = mat
        self.stroke_id = stk_id
        self.segment_id = seg_id
        self.is_line = is_line

    def get_material(self):
        return bpy.data.materials[self.material]
    
    def get_object(self):
        return bpy.data.objects[self.object]
    
    def get_layer(self):
        return self.get_object().data.layers[self.layer]
    
    def get_stroke(self):
        return self.get_layer().active_frame.strokes[self.stroke_id]

classes = [GPEYEDROPPER_PickedItem]
def register():
    for cls in classes:
        bpy.utils.register_class(cls) 

def unregister():        
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
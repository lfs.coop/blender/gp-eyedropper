    
''' --- Class Registration --- '''
def register(addon_keymaps):   
    from . eyedropper_props import register as register_props
    register_props()

    from . eyedropper_ops import register as register_ops
    register_ops(addon_keymaps)

def unregister():      
    from . eyedropper_ops import unregister as unregister_ops
    unregister_ops() 

    from . eyedropper_props import unregister as unregister_props
    unregister_props()


if __name__ == "__main__":
    register() 

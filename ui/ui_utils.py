import gpu, bgl, blf
from gpu_extras.batch import batch_for_shader
import numpy as np
from math import pi, cos, sin

def gray_rgb(g):
    return 3*[g] + [1.]

def sample_circle(npts=20, center=(0,0), radius=1, th0=0, th1=2*pi):
    return [ (center[0] + radius*cos(th), center[1] + radius*sin(th)) \
            for th in np.linspace(th0, th1, npts) ]
    
def gpu_draw_mesh(verts, faces, color, fill=True):    
    shader = gpu.shader.from_builtin('UNIFORM_COLOR')
    if fill:
        mode = 'TRIS'
        indices = faces
    else:
        mode = 'LINES'
        indices = [ [ (t[i-1],t[i]) for i in range(len(t)) ] for t in faces]
        indices = list({ k for x in indices for k in x })
    batch = batch_for_shader(shader, mode, {"pos": verts}, indices=indices)
    shader.bind()
    shader.uniform_float("color", color)
    batch.draw(shader)
    return shader

def gpu_rectangle(pos, dim):
    x,y = pos
    wdt,hgt = dim
    fverts = ( (x,y), (x + wdt,y), (x, y + hgt), (x + wdt, y + hgt))
    indices = ((0, 1, 2), (2, 1, 3))
    return fverts,indices

def gpu_rounded_rectangle(pos, dim, rds, pts_per_corner = 5):
    x,y = pos
    wdt,hgt = dim
    V,F = [], []

    # main polygon
    px0,px1,px2,px3 = (x, x+rds, x+wdt-rds, x+wdt)
    py0,py1,py2,py3 = (y, y+rds, y+hgt-rds, y+hgt)
    V += [ \
        (px1,py0), (px2,py0), (px2,py1), \
        (px3,py1), (px3,py2), (px2,py2), \
        (px2,py3), (px1,py3), (px1,py2), \
        (px0,py2), (px0,py1), (px1,py1) \
        ]
    F += [ (0,1,7), (1,6,7), (2,3,5), (3,4,5), (10,11,9), (11,8,9) ]

    # rounded corners
    def add_rounded_corner(center_id, angle_interval):
        nonlocal V, F
        vert_nb = len(V)
        ctr = V[center_id]
        th0,th1 = angle_interval
        cr_verts = sample_circle(center=ctr, radius=rds, npts=pts_per_corner, th0=th0, th1=th1)
        cr_ind = [ (center_id, vert_nb+i, vert_nb+i+1) for i in range(pts_per_corner-1) ]
        
        V += cr_verts
        F += cr_ind

    add_rounded_corner(2, (-pi/2,0))
    add_rounded_corner(5, (0,pi/2))
    add_rounded_corner(8, (pi/2,pi))
    add_rounded_corner(11,(pi,3*pi/2))

    return V,F

def gpu_get_preview_texture(item, check_custom=False, use_icon=False):
    prv = item.preview
    if check_custom and (not item.use_custom_icon):
        return None
    elif not prv:
        item.asset_generate_preview()
        print(f"ERROR : Item {item.name} has no preview image")
        return None

    if use_icon:
        s = prv.icon_size
        dat = prv.icon_pixels_float
    else:
        s = prv.image_size
        dat = prv.image_pixels_float

    # data as a list : accelerates by far the buffer loading
    dat = [ d for d in dat ] 
    ts = s[0]*s[1]*4
    if ts < 1:
        item.asset_generate_preview()
        print(f"ERROR : Could not load mat {item.name} preview image")
        return None

    if all([ (d==0) for d in dat ]):
        item.asset_generate_preview()
        print(f"ERROR : Could not load mat {item.name} preview image")
        return None

    import gpu
    pbf = gpu.types.Buffer('FLOAT', ts, dat)
    return gpu.types.GPUTexture(s, data=pbf, format='RGBA16F')

def gpu_draw_texture(texture, pos):
    gpu.state.blend_set('ALPHA')   
    from gpu_extras.presets import draw_texture_2d
    draw_texture_2d(texture, pos, texture.width, texture.height)     
    gpu.state.blend_set('NONE')      

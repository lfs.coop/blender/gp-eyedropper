import blf
from . ui_utils import gray_rgb, gpu_get_preview_texture
class GridItem(): 
    width, height = 0,0
    
    def draw(self, pos):
        pass

class TextGridItem(GridItem):
    def __init__(self, text, color = gray_rgb(0.5), font_id = 0, font_size=12):
        self.text = text
        self.font_id = font_id
        self.font_size = font_size
        self.color = color

        blf.size(self.font_id, self.font_size)
        self.width, self.height = blf.dimensions(self.font_id, self.text)

        global _font_id_count
        _font_id_count = font_id+1
        
    def draw(self, context, pos):        
        blf.size(self.font_id, self.font_size)
        blf.color(self.font_id, *self.color)
        blf.position(self.font_id, pos[0], pos[1], 0)
        blf.draw(self.font_id, self.text)

class MaterialIconItem(GridItem):
    def __init__(self, material):
        self.mat = material
        self.gpu_texture = None
        self.use_preview = False

        self.stroke_color = 4*[0.]
        self.fill_color = 4*[0.]
        if (material is None) \
            or (not material.is_grease_pencil):
            return
        gpmat = material.grease_pencil

        self.is_flat = not ((gpmat.show_stroke and (gpmat.stroke_style != 'SOLID')) \
                            or (gpmat.show_fill and (gpmat.fill_style != 'SOLID')))
        if not self.is_flat:
            self.gpu_texture = gpu_get_preview_texture(self.mat,use_icon=True)

        self.use_preview = (not self.gpu_texture is None)

        if self.use_preview:
            self.width = self.gpu_texture.width
            self.height = self.gpu_texture.height
        else:
            if gpmat.show_stroke:
                self.stroke_color = [ c for c in gpmat.color ]
            if gpmat.show_fill:
                self.fill_color = [ c for c in gpmat.fill_color ]

    def set_radius(self, rds):
        self.width = rds*2
        self.height = rds*2
        self.radius = rds
    
    def get_centered(self, context, pos):
        rds = self.radius
        rgw, rgh = context.region.width, context.region.height
        return (pos[0] - 0.5*rgw + rds, pos[1] - 0.5*rgh + rds)

    def draw_preview(self, context, pos):
        cpos = self.get_centered(context, pos)
        rds = self.radius     
        from . ui_gpu_draw import draw_gpu_texture
        draw_gpu_texture(self.gpu_texture, 2*rds, cpos, \
            convert_srgb=True)

    def draw_flat(self, context, pos):
        cpos = self.get_centered(context, pos)
        rds = self.radius     
        from . ui_gpu_draw import draw_flat_circle
        draw_flat_circle(cpos, rds, \
            fill_color=self.fill_color, line_color=self.stroke_color, \
            line_width=0.3*rds)

    def draw(self, context, pos):
        if self.use_preview:
            draw_func = self.draw_preview
        else:
            draw_func = self.draw_flat
        draw_func(context, pos)
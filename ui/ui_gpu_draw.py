# GPU drawing related functions
import gpu
from gpu_extras.batch import batch_for_shader
import numpy as np

''' Common GLSL functions used in many shaders 
    Mostly creates geometric primitives with anti-aliasing
'''
common_libsh='''
    vec4 srgb_to_linear_rgb(vec4 c){
        vec4 sc = c;
        sc.r = (c.r < 0.04045)?(c.r/12.92):( pow( (c.r+0.055)/1.055, 2.4) );
        sc.g = (c.r < 0.04045)?(c.g/12.92):( pow( (c.g+0.055)/1.055, 2.4) );
        sc.b = (c.r < 0.04045)?(c.b/12.92):( pow( (c.b+0.055)/1.055, 2.4) );
        return sc;
    }

    float aa_circle(float rds, float dst, float eps){
        return smoothstep(rds+eps, rds-eps, dst);
    }        

    float aa_contour(float rds, float wdt, float dst, float eps){
        float a0 = aa_circle(rds+wdt/2., dst, eps);
        float a1 = aa_circle(rds-wdt/2., dst, eps);
        return a0*(1-a1);
    }     

    float aa_donut(float rds0, float rds1, float dst, float eps){
        float a0 = aa_circle(rds0, dst, eps);
        float a1 = aa_circle(rds1, dst, eps);
        return a0*(1-a1);
    }

    float aa_seg( vec2 s0, vec2 s1, vec2 p, float wdt, float eps){
        float lgt = length(s0-s1);
        vec2 udr = (s1-s0)/lgt;
        vec2 lp = p - s0;
        float prj = dot(lp, udr);
        float alpha = 1.;

        alpha *= smoothstep(-eps, eps, prj);  
        alpha *= smoothstep(lgt+eps, lgt-eps, prj);  
        if( alpha == 0.){
            return 0.;
        }
        
        float d = length(prj*udr - lp);
        return alpha*smoothstep(wdt+eps, wdt-eps, d);
    }

    vec4 alpha_compose(vec4 A, vec4 B){
        /* A over B */
        vec4 color = vec4(0.);
        color.a = A.a + B.a*(1.- A.a);
        if( color.a == 0. ){
            return color;
        }
        color.rgb = (A.rgb * A.a + B.rgb * B.a * (1 - A.a))/(color.a);
        return color;
    }

    bool in_interval(float x, float a, float b){
        return (x >= a) && (x <= b);
    }
'''

''' Sets up a shader program to draw an image in the dimensions given in the settings '''
def setup_drawing_shader(origin, dimension, fragsh, libsh=common_libsh):
    vsh = '''            
        uniform mat4 modelViewProjectionMatrix;
        uniform float dimension;
        uniform vec2 origin;
        
        in vec2 pos;
        out vec2 lpos;
        out vec2 uv;

        void main()
        {
          gl_Position = modelViewProjectionMatrix*vec4(dimension*pos+origin, 0.0, 1.0);
          lpos = dimension*pos;
          uv = (pos+1)*0.5;
        }
    '''
    shader = gpu.types.GPUShader(vsh, fragsh, libcode=libsh)

        # Simple screen quad
    vdata = np.asarray(((-1, 1), (-1, -1),(1, -1), (1, 1)))
    vertices = np.ndarray.tolist(vdata)        
    indices = ((0, 1, 2), (0, 2, 3))
    
    batch = batch_for_shader(shader, 'TRIS', {"pos": vertices}, indices=indices)
    shader.bind() 
    
        # Set up uniform variables
    matrix = gpu.matrix.get_projection_matrix()*gpu.matrix.get_model_view_matrix()
    shader.uniform_float("modelViewProjectionMatrix", matrix)     
    shader.uniform_float("dimension",dimension*0.5)
    shader.uniform_float("origin", origin)

    return shader,batch 

def draw_flat_circle(center, radius, \
    fill_color=4*[0.], line_color=4*[0.], line_width=1., aa_eps=0.2):

    flat_prv_fsh = '''
        #define PI 3.1415926538
        uniform vec4 fill_color;
        uniform vec4 line_color;
        uniform float mat_line_width;
        uniform float radius;
        uniform float aa_eps;

        in vec2 lpos;
        in vec2 uv;
        out vec4 fragColor;   

        void main()
        {          
            float d = length(lpos);    

            if( d > radius + aa_eps*2 ){
                fragColor = vec4(0.);
                return;
            }

            vec4 fcolor = fill_color;
            vec4 lcolor = line_color;
            fcolor.a *= aa_circle(radius, d, aa_eps);
            lcolor.a *= aa_contour(radius, mat_line_width, d, aa_eps);

            fragColor = alpha_compose(lcolor, fcolor);
        }
    '''
    gpu.state.blend_set('ALPHA')   
    dimension = radius*2 + line_width
    shader, batch = setup_drawing_shader(center, dimension, fragsh=flat_prv_fsh)
    shader.uniform_float("fill_color", fill_color)
    shader.uniform_float("line_color", line_color)
    shader.uniform_float("mat_line_width", line_width)
    shader.uniform_float("radius", radius)   
    shader.uniform_float("aa_eps",aa_eps)    
    batch.draw(shader)
    gpu.state.blend_set('NONE')   


def draw_gpu_texture(texture, dimension, position=[0,0],convert_srgb=False):
    gpu_tex_fsh = '''
        #define PI 3.1415926538
        uniform sampler2D tex;  
        uniform bool convert_srgb;
        uniform float dimension;

        in vec2 lpos;
        in vec2 uv;
        out vec4 fragColor;      

        void main()
        {        
            float aspect_ratio = textureSize(tex,0).x / float(textureSize(tex,0).y);
            float w = 2*dimension;
            float h = 2*dimension;
            if(aspect_ratio > 1){
                w *= aspect_ratio;
            }
            else{
                h /= aspect_ratio;
            }
            vec2 uv_tex = lpos/(vec2(w,h)) + vec2(0.5);

            fragColor = texture(tex,uv_tex);
            
            if(convert_srgb){
                fragColor = srgb_to_linear_rgb(fragColor);
            }
        }
    '''
    gpu.state.blend_set('ALPHA')   
    shader, batch = setup_drawing_shader(position, dimension*1.2, fragsh=gpu_tex_fsh)
    shader.uniform_sampler("tex", texture)
    shader.uniform_bool("convert_srgb", [convert_srgb])   
    batch.draw(shader)
    gpu.state.blend_set('NONE')

from . ui_utils import *
from . ui_grid_item import *

class UIGridTheme():
    back_inner = gray_rgb(0.1)
    back_roundness = 0.4
    item_text = gray_rgb(0.9)
    text_size = 11

def grid_theme_from_preferences(context):
    grid_theme = UIGridTheme()

    themes = context.preferences.themes
    if themes:
        theme = themes[0]    
        theme_menu_back = theme.user_interface.wcol_menu_back
        grid_theme.back_inner = theme_menu_back.inner
        grid_theme.back_roundness = theme_menu_back.roundness
        theme_menu_item = theme.user_interface.wcol_menu_item
        grid_theme.item_text = [c for c in theme_menu_item.text] + [1]

    ui_styles = context.preferences.ui_styles
    if ui_styles:
        ui_style = ui_styles[0]
        ui_widget_font_size = ui_style.widget.points
        grid_theme.text_size = ui_widget_font_size

    return grid_theme
    
class UIGrid():
    def __init__(self, ncol=1, nrow=1, theme=UIGridTheme(), padding=15):
        self.ncol = ncol
        self.nrow = nrow

        self.items = nrow * [ ncol * [ GridItem() ] ]

        self.row_padding = padding
        self.col_padding = padding

        self.theme = theme

        self.update_column_width()
        self.update_row_height()

    def set_item(self, row, col, item):
        self.items[row][col] = item
        self.update_column_width(col)
        self.update_row_height()

    def set_text(self, row, col, text):
        textit = TextGridItem( \
                    text=text, \
                    font_size=self.theme.text_size, \
                    color = self.theme.item_text)            
        self.set_item(row, col, textit)

    def set_material(self, row, col, material):
        matit = MaterialIconItem( \
                material=material)
        matit.set_radius(self.row_height*0.5)         
        self.set_item(row, col, matit)

    def update_column_width(self, col_id = -1):
        def col_width(col_id):
            return max([ (row[col_id].width) \
                            for row in self.items ])
        if col_id < 0:
            self.col_width = [ col_width(c) for c in range(self.ncol) ]
        else:          
            self.col_width[col_id] = col_width(col_id)

    def update_row_height(self):
        row_heights = [ item.height \
                        for row in self.items for item in row]
        self.row_height = max(row_heights)        
     
    def get_width(self):
        return sum( [ w + self.col_padding for w in self.col_width ])
    
    def get_height(self):
        return self.nrow * (self.row_height + self.row_padding)

    def draw_frame(self, cursor):        
        dim = self.get_width(), self.get_height()
        rH = self.row_height+self.row_padding
        roundness = 0.5*rH*self.theme.back_roundness
        fverts, indices = gpu_rounded_rectangle(cursor, dim, rds=roundness)
        gpu_draw_mesh(fverts, indices, self.theme.back_inner)

    def draw_cells(self, context, cursor):
        cell_h = self.row_height
        iy = cursor[1] + self.row_padding*0.5
        for row in self.items:
            ix = cursor[0] + self.col_padding*0.5
            for col_id,item in enumerate(row):
                cell_w = self.col_width[col_id]
                item_w,item_h = item.width, item.height
                # centering the item in cell
                item_pos = (ix + 0.5*(cell_w-item_w), iy + 0.5*(cell_h-item_h))
                item.draw(context, item_pos)
                ix += cell_w + self.col_padding
            iy += cell_h + self.row_padding  

    def draw(self, context, cursor):
        self.draw_frame(cursor)
        self.draw_cells(context, cursor) 
# Grease Pencil Eyedropper

The eyedropper is a tool that allows you to see and select the grease pencil features that are under your cursor at any time.
It can be used in two modes : 
 - an on-fly mode to quickly switch to a specific layer and material, and
 - an on-click mode to display and manipulate all layers and materials under the cursor.

![Eyedropper Modes overview](doc/eyedropper_modes.png "Overview of the 2 modes of the GP Eyedropper")

## ON-FLY MODE
![ON-FLY Eyedropper Mode](doc/onfly_eyedrop.gif)

Press and maintain D to invoke the eyedropper in fly mode. The object/layer/material of the visible stroke directly under the cursor should be displayed in a small popup. This data will be updated while you move the cursor.
You can select the object and layer and material by Left-Clicking. If you release S without clicking, then nothing will be selected.

## ON-CLICK MODE
![ON-CLICK Eyedropper Mode](doc/onclick_eyedrop.png)

Click on D to invoke the eyedropper in click mode. The object/layer/material of all strokes under the cursor should be listed, in visibility order.
As for fly mode, you can select an object/layer/material by clicking on an element of the list. The icons in the right side of the panel offers more option : 
 - toggle visibility of a layer,
 - select an object/layer (but not the material),
 - or pick a material (but not the object/layer).

## Known Limitations
The eyedropper will precisely detect stroke boundaries only for objects with Stroke Thickness mode set to 'World Space' (it is the default value).

Plus, it will not (yet) detect any changes in width caused by thickness modifiers.

## License
Published under GPLv3 license.
